const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.postsDocumentCreated = functions.firestore.document('products/{productId}')
    .onCreate((snap, context) => {
        const newValue = snap.data();

        const ts = admin.firestore.Firestore.Timestamp.now();

        return snap.ref.update({
            ...newValue,
            created_date: ts
        });

    })