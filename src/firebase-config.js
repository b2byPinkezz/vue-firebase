import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyAcwZqY7xju4NaVYdPbuUyS9kyGx2dfmW4",
  authDomain: "my-test-78bb2.firebaseapp.com",
  databaseURL: "https://my-test-78bb2.firebaseio.com",
  projectId: "my-test-78bb2",
  storageBucket: "my-test-78bb2.appspot.com",
  messagingSenderId: "34990446004",
  appId: "1:34990446004:web:e31337387d249be4"
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const auth = firebase.auth();
let currentUser = null;


const storageRef = firebase.storage();

const googleProvider = new firebase.auth.GoogleAuthProvider();

const setUser = (user) => {
  currentUser = user;
};

const products = db.collection('products');


export {
  db,
  auth,
  products,
  setUser,
  currentUser,
  storageRef,
  googleProvider
}