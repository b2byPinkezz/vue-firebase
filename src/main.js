import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {auth} from './firebase-config';
import store from './store'
import { Button, Select, Input } from 'element-ui'

Vue.use(Button);
Vue.use(Select);
Vue.use(Input);


Vue.config.productionTip = false


auth.onAuthStateChanged(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})

