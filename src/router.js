import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import auth from './middleware/auth';
import { currentUser } from './firebase-config';

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/firebase',
      name: 'firebase',
      beforeEnter: function (to, from, next) {
        if (!currentUser) {
            return next({name: 'home'})
        }
        return next();
    },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
